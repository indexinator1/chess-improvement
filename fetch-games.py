import requests
import pandas as pd
from io import StringIO
import chess.pgn

username = 'indexinator'
api_url = f'https://lichess.org/api/games/user/{username}'
params = {
    'rated': 'true',
    'perfType': 'blitz',
    'analysed': 'true',
    'clocks': 'true',
    'evals': 'true',
}

try:
    response = requests.get(api_url, params=params)
    if response.status_code == 200:
        games_text = response.text
        pgn = StringIO(games_text) # Simulate file-like object
        
        games = []
        
        while True:
            # https://python-chess.readthedocs.io/en/latest/pgn.html#parsing
            game = chess.pgn.read_game(pgn)
            if game is None:
                break  # No more games to read

            moves = []
            node = game
            board = chess.Board()
            
            # Iterate through all the moves in the game
            for i, move in enumerate(game.mainline_moves(), start=1):
                next_node = node.variation(0)
                
                # get the clock time and evaluation for the move
                # do this in a safe way
                try:
                    # https://python-chess.readthedocs.io/en/latest/pgn.html#chess.pgn.Game.headers
                    game_id = game.headers["Site"].split('/')[-1]
                    white = game.headers["White"]
                    black = game.headers["Black"]
                    move_number = str(next_node.ply())
                    
                    if node.board().turn:
                        player = white
                    else:
                        player = black
                    
                    # https://python-chess.readthedocs.io/en/latest/pgn.html#chess.pgn.GameNode
                    # once you get the move in UCI notation
                    # you get the SAN (what you expect) notation of the move and update the board
                    uci_move = next_node.move.uci()
                    move = chess.Move.from_uci(uci_move)
                    san_move = board.san(move)
                    board.push(move)
                    
                    clock_time = next_node.clock()
                    try:
                        evaluation = next_node.eval().white().score()
                    except:
                        evaluation = None # no valuation if mate in x
                except:
                    game_id = None
                    white = None
                    black = None
                    move_number = None
                    player = None
                    move = None
                    clock_time = None
                    evaluation = None
                
                moves.append({"game_id": game_id,
                              "move_number": move_number,
                              "player": player,
                              "move": san_move,
                              "clock_time": clock_time,
                              "evaluation": evaluation})
                
                node = next_node
        
            games.append(moves)

        # games is an array of games, where each game is an array of moves
        # each move is a dictionary
        # convert games to a single array of moves
        moves = [move for game in games for move in game]
        
        # save moves as csv using pandas
        df = pd.DataFrame(moves)
        df.to_csv('games.csv', index=False)
    else:
        print(f"Failed to fetch games. Status code: {response.status_code}")
except Exception as e:
    print(f"An error occurred: {e}")